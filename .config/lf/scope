#!/bin/bash

set -C -f -u
#IFS=$'\n'
IFS="$(printf '%b_' '\n')"; IFS="${IFS%_}"

# ANSI color codes are supported.
# STDIN is disabled, so interactive scripts won't work properly

# This script is considered a configuration file and must be updated manually.

# Meanings of exit codes:
# code | meaning    | action of ranger
# -----+------------+-------------------------------------------
# 0    | success    | Display stdout as preview
# 1    | no preview | Display no preview at all
# 2    | plain text | Display the plain content of the file

# Script arguments
FILE_PATH="${1}"         # Full path of the highlighted file
HEIGHT="${2}"

#FILE_EXTENSION="${FILE_PATH##*.}"
#FILE_EXTENSION_LOWER=$(echo ${FILE_EXTENSION} | tr '[:upper:]' '[:lower:]')

# Settings
HIGHLIGHT_SIZE_MAX=262143  # 256KiB
HIGHLIGHT_TABWIDTH=8
HIGHLIGHT_STYLE='pablo'


handle_mime() {
	local mimetype="${1}"
	case "${mimetype}" in
	# HTML
	text/html) lynx -dump -- "${FILE_PATH}" ;;

	# MAN Pages 
	text/troff) man ./ "${FILE_PATH}" | col -b ;;

	# CVS
	application/csv) column -s, -t < "${FILE_PATH}" ;;

	# General text
	text/* | */xml)
		if [ "$( stat --printf='%s' -- "${FILE_PATH}" )" -gt "${HIGHLIGHT_SIZE_MAX}" ]; then
			exit 2
		fi
		if [ "$( tput colors )" -ge 256 ]; then
			local highlight_format='xterm256'
		else
			local highlight_format='ansi'
		fi
			highlight --replace-tabs="${HIGHLIGHT_TABWIDTH}" --out-format="${highlight_format}" \
			--style="${HIGHLIGHT_STYLE}" --force -- "${FILE_PATH}" && exit || exit 1 ;;

	# JSON
	application/json) jq --color-output . "${FILE_PATH}" ;;

	# ZIP
	application/zip) atool --list -- "${FILE_PATH}" ;;

	# IMAGE
	image/*) chafa --fill=block --symbols=block -c 256 -s 80x"${HEIGHT}" "${FILE_PATH}" || exit 1;;

	# VIDEO & OTHER MEDIA
	video/*) chafa --fill=block --symbols=block -c 256 -s 80x"${HEIGHT}" "$(vthumbnail ${FILE_PATH})"  exit 1;;
	audio/*|application/octet-stream) mediainfo "${FILE_PATH}" || exit 1;;

	# PDF
	*/pdf) pdftotext -l 10 -nopgbrk -q -- "${FILE_PATH}" - ;;

	# OpenDocument
	*opendocument*) odt2txt "${FILE_PATH}" ;;
	esac
}

handle_fallback() {
	echo '----- File Type Classification -----' && file --dereference --brief -- "${FILE_PATH}"
	exit 1
}


# Get mimetype of file
MIMETYPE="$( file --dereference --brief --mime-type -- "${FILE_PATH}" )"
handle_mime "${MIMETYPE}"
handle_fallback

exit 1
