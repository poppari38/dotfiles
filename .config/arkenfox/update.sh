#!/bin/sh

#[ ! -d "$HOME/.cache/arkenfox" ] && mkdir -p "$HOME/.cache/arkenfox"
#diffname="update.$(date +%Y-%m-%d.%H:%M:%S).diff"

arkenfox-updater \
	-p "$HOME/.config/firefox" \
	-o "$HOME/.config/arkenfox/user-overrides.js" \
	-b \
	-c 

# -p is Absolute firefox profile location
# -o Ovveride file
# -b only one backup
# -c is create diff file
	

# Change into 
cd "$HOME/.config/firefox"
arkenfox-cleaner -s

