// Override file for arkenfox user.js
// by poppari38
// Threat model is privacy first + sec
//

// 0401: disable SB (Safe Browsing)
user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.safebrowsing.phishing.enabled", false);

// 0402: disable SB checks for downloads (both local lookups + remote)
user_pref("browser.safebrowsing.downloads.enabled", false);

// 0701: re-enable IPv6
user_pref("network.dns.disableIPv6", true);

// 0801: re-renable location bar search (quality of life)
user_pref("keyword.enabled", true);

// 1001: re-enable disk cache (maybe gives performance)
user_pref("browser.cache.disk.enable", true);

// 2031: re-enable autoplay of media if you interacted with the site [FF78+]
// reason of 4chan-x
user_pref("media.autoplay.blocking_policy", 0);

// 5010: remove topsites from urlbar
user_pref("browser.urlbar.suggest.topsites", false);

// 5003: disable password saving
user_pref("signon.rememberSignons", false);

// 2803: clear on shutdown stuff
user_pref("privacy.clearonshutdown.cache", false);
user_pref("privacy.clearonshutdown.cookies", false);

// 4504: re-disable RFP letterboxing [FF67+]
user_pref("privacy.resistFingerprinting.letterboxing", false); 

// 7002: set default permissions (disable notificiations if annoying?)
 // 0=always ask (default), 1=allow, 2=block
   // user_pref("permissions.default.geo", 0);
   // user_pref("permissions.default.camera", 0);
   // user_pref("permissions.default.microphone", 0);
   // user_pref("permissions.default.desktop-notification", 0);
   // user_pref("permissions.default.xr", 0); // Virtual Reality


// KEYBINDS

// disable hardware media keys
user_pref("media.hardwaremediakeys.enabled", false);

// make ctrl-tab usable
user_pref("browser.ctrlTab.sortByRecentlyUsed", false);


// FEATURES

// disable pocket [FF46+]
user_pref("extensions.pocket.enabled", false);

// enable userchrome.css
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true); // [FF68+] allow userChrome/userContent


// LINUX

// enable middle-click auto-scrolling [Linux]
user_pref("general.autoScroll", true);

// make right click work with dwm (fixes instant context menu) (not needed)
// user_pref("ui.context_menus.after_mouseup", true);


// BOOKMARKS

// don't show toolbar bookmarks (del?)
// user_pref("browser.toolbars.bookmarks.visibility", "never");

// disable from toolbar bookmarks other bookmarks
//user_pref("browser.toolbars.bookmarks.showOtherBookmarks", false);

// limit bookmark backups to 2
user_pref("browser.bookmarks.max_backups", 2);

// open bookmarks in a new tab [ff57+]
// user_pref("browser.tabs.loadBookMarksInTabs", true);


// VISUAL

// toolbar always visible in fullscreen mode
user_pref("browser.fullscreen.autohide", false);
