"
" poppari38's nvim config file
"

if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/plugged"'))
Plug 'tpope/vim-surround'
Plug 'preservim/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'tpope/vim-commentary'
Plug 'dracula/vim',{'as':'dracula'}
Plug 'ap/vim-css-color'
" Telescope
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.1' }
call plug#end()

" Visual things
set number
set cc=80
set background=dark
" Tabs seem like 4 spaces
set tabstop=4
set shiftwidth=4
" Disable swap and backup files
set nobackup
set noswapfile
set noundofile
" Make mouse work in all modes
set mouse=a
" Other
set nohlsearch " Don't keep highlighted after search
set title
set go=a " Clipboard autocopy in gui mode
set clipboard=unnamedplus
filetype plugin on
syntax on

colorscheme dracula

" Enable autocompletion:
set wildmode=longest,list,full
" Disables automatic commenting on newline:
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
